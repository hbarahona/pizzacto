class AddAccountToTrabajador < ActiveRecord::Migration[5.1]
  def change
    add_reference :trabajador, :account, foreign_key: true
  end
end
