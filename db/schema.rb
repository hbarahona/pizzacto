# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170704091428) do

  create_table "account", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: 6
    t.datetime "remember_created_at", precision: 6
    t.integer "sign_in_count", precision: 38, default: 0, null: false
    t.datetime "current_sign_in_at", precision: 6
    t.datetime "last_sign_in_at", precision: 6
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "rol"
    t.index ["email"], name: "index_account_on_email", unique: true
    t.index ["reset_password_token"], name: "i_account_reset_password_token", unique: true
  end

  create_table "audit_tables", id: false, force: :cascade do |t|
    t.string "aud_trans", limit: 1
    t.string "aud_table", limit: 20
    t.date "aud_fecha"
    t.string "aud_user", limit: 20
    t.integer "aud_id", limit: 3, precision: 3
    t.string "aud_old", limit: 30
    t.string "aud_new", limit: 30
  end

  create_table "cliente", primary_key: "cli_id", force: :cascade, comment: "Clientes" do |t|
    t.string "cli_nomb", limit: 30, null: false, comment: "Nombre"
    t.string "cli_appa", limit: 30, comment: "Apellido Paterno"
    t.string "cli_apma", limit: 30, comment: "Apellido Materno"
    t.string "cli_rut", limit: 10, comment: "Rut"
    t.string "cli_pass", limit: 10, comment: "Contrase?a"
    t.integer "account_id", precision: 38
    t.index ["account_id"], name: "index_cliente_on_account_id"
  end

  create_table "estado", primary_key: "est_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Estado del pedido" do |t|
    t.string "est_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "ins_pro", primary_key: ["inpr_prod", "inpr_insu"], force: :cascade, comment: "Insumos por producto" do |t|
    t.integer "inpr_prod", limit: 3, precision: 3, null: false, comment: "Producto"
    t.integer "inpr_insu", limit: 3, precision: 3, null: false, comment: "Insumo"
    t.integer "inpr_cant", limit: 5, precision: 5, null: false, comment: "Cantidad de insumo utilizado"
    t.string "inpr_med", limit: 3, comment: "Unidad de medida"
  end

  create_table "insumo", primary_key: "ins_id", force: :cascade, comment: "Insumos" do |t|
    t.string "ins_nomb", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "medida", primary_key: "med_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Determina la medida Kilogramos o Unidades" do |t|
    t.string "med_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "pedido", primary_key: "ped_id", force: :cascade, comment: "Pedidos" do |t|
    t.integer "ped_tota", limit: 7, precision: 7, null: false, comment: "Valor total en pesos"
    t.date "ped_fecha", null: false, comment: "Fecha"
    t.string "ped_esta", limit: 3, null: false, comment: "Estado"
    t.integer "ped_pizz", limit: 3, precision: 3, null: false, comment: "Pizzeria"
    t.string "ped_trab", limit: 10, null: false, comment: "Trabajador"
    t.integer "ped_clie", limit: 6, precision: 6, null: false, comment: "Cliente"
  end

  create_table "pizzeria", primary_key: "piz_id", force: :cascade, comment: "Pizzerias" do |t|
    t.string "piz_nomb", limit: 50, null: false, comment: "Nombre"
    t.string "piz_dire", limit: 50, null: false, comment: "Direccion"
    t.integer "piz_tele", limit: 9, precision: 9, null: false, comment: "Telefono"
  end

  create_table "pro_ped", primary_key: ["prpe_prod", "prpe_pedi"], force: :cascade, comment: "Productos por pedido" do |t|
    t.integer "prpe_prod", limit: 7, precision: 7, null: false, comment: "Pedido"
    t.integer "prpe_pedi", limit: 3, precision: 3, null: false, comment: "Producto"
    t.integer "prpe_cant", limit: 3, precision: 3, null: false, comment: "Cantidad del Producto"
  end

  create_table "producto", primary_key: "pro_id", force: :cascade, comment: "Productos de la Pizzeria" do |t|
    t.string "pro_desc", limit: 30, null: false, comment: "Descripcion"
    t.integer "pro_prec", limit: 5, precision: 5, null: false, comment: "Precio en pesos"
    t.string "pro_tpro", limit: 3, null: false, comment: "Tipo"
  end

  create_table "resumen_ventas", primary_key: "rsv_cod_local", force: :cascade do |t|
    t.string "rsv_nom_local", limit: 50
    t.integer "rsv_vendedores", limit: 3, precision: 3
    t.integer "rsv_ventas", limit: 7, precision: 7
    t.integer "rsv_productos", limit: 7, precision: 7
    t.integer "rsv_dias", limit: 3, precision: 3
    t.string "rsv_usuario", limit: 20, comment: "Usuario (esquema) que ejecuta el procedimiento"
    t.string "rsv_ip", limit: 20
    t.date "rsv_fecha"
  end

  create_table "stock", primary_key: "sto_id", force: :cascade, comment: "Ingreso de insumos" do |t|
    t.date "sto_fing", null: false, comment: "Fecha de ingreso"
    t.date "sto_fcau", null: false, comment: "Fecha de caducidad"
    t.integer "sto_cant", limit: 4, precision: 4, null: false, comment: "Cantidad"
    t.string "sto_medi", limit: 3, null: false, comment: "Unidad de medida (Kilo o Unidades)"
    t.integer "sto_prec", limit: 7, precision: 7, null: false, comment: "Precio en pesos"
    t.integer "sto_insu", limit: 3, precision: 3, null: false, comment: "Insumo"
    t.integer "sto_pizz", limit: 3, precision: 3, null: false, comment: "Pizzeria"
  end

  create_table "tipo_producto", primary_key: "tpro_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Tipos de Producto" do |t|
    t.string "tpro_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "tipo_trabajador", primary_key: "ttra_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Cargo del trabajador" do |t|
    t.string "ttra_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "trabajador", primary_key: "tra_rut", id: :string, limit: 10, force: :cascade, comment: "Trabajadores" do |t|
    t.string "tra_nomb", limit: 30, null: false
    t.string "tra_appa", limit: 30, null: false
    t.string "tra_apma", limit: 30, null: false
    t.string "tra_pass", limit: 10, null: false
    t.integer "tra_pizz", limit: 3, precision: 3, null: false
    t.string "tra_ttra", limit: 3, null: false
    t.integer "trabajador_id", precision: 38
    t.integer "account_id", precision: 38
    t.index ["account_id"], name: "index_trabajador_on_account_id"
    t.index ["trabajador_id"], name: "i_trabajador_trabajador_id"
  end

  add_foreign_key "cliente", "account"
  add_foreign_key "ins_pro", "insumo", column: "inpr_insu", primary_key: "ins_id", name: "fk2_ins_pro"
  add_foreign_key "ins_pro", "medida", column: "inpr_med", primary_key: "med_id", name: "fk3_ins_pro"
  add_foreign_key "ins_pro", "producto", column: "inpr_prod", primary_key: "pro_id", name: "fk1_ins_pro"
  add_foreign_key "pedido", "cliente", column: "ped_clie", primary_key: "cli_id", name: "fk4_pedido"
  add_foreign_key "pedido", "estado", column: "ped_esta", primary_key: "est_id", name: "fk1_pedido"
  add_foreign_key "pedido", "pizzeria", column: "ped_pizz", primary_key: "piz_id", name: "fk2_pedido"
  add_foreign_key "pedido", "trabajador", column: "ped_trab", primary_key: "tra_rut", name: "fk3_pedido"
  add_foreign_key "pro_ped", "pedido", column: "prpe_pedi", primary_key: "ped_id", name: "fk1_pro_ped"
  add_foreign_key "pro_ped", "producto", column: "prpe_prod", primary_key: "pro_id", name: "fk2_pro_ped"
  add_foreign_key "producto", "tipo_producto", column: "pro_tpro", primary_key: "tpro_id", name: "fk_producto"
  add_foreign_key "stock", "insumo", column: "sto_insu", primary_key: "ins_id", name: "fk1_stock"
  add_foreign_key "stock", "medida", column: "sto_medi", primary_key: "med_id", name: "fk3_stock"
  add_foreign_key "stock", "pizzeria", column: "sto_pizz", primary_key: "piz_id", name: "fk2_stock"
  add_foreign_key "trabajador", "account"
  add_foreign_key "trabajador", "pizzeria", column: "tra_pizz", primary_key: "piz_id", name: "fk1_trabajador"
  add_foreign_key "trabajador", "tipo_trabajador", column: "tra_ttra", primary_key: "ttra_id", name: "fk2_trabajador"
end
