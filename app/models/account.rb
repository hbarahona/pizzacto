class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :cliente
  has_one :trabajador
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  attribute :rol, :string, default: "cli"

  after_create :crearcliente

  def crearcliente
    if (self.rol == nil || self.rol == "cli" )
      @clie =Cliente.create(cli_nomb: "Nombre", cli_pass: self.password, account_id:self.id)
      @clie.save
    end
  end
end
