class Trabajador < ApplicationRecord
  belongs_to :local, :foreign_key => :tra_pizz, :primary_key => :piz_id
  belongs_to :tipo_trabajador, :foreign_key => :tra_ttra, :primary_key => :ttra_id
  has_many :pedido, :foreign_key => :ped_trab, :primary_key => :tra_rut
  belongs_to :account

  validates :tra_rut, uniqueness: true

  before_validation :crearcuenta

  def crearcuenta
    @trabemail= self.tra_nomb+"@pizzacto.cl"
    @acctrab = Account.create(email: @trabemail, password: "password")
    self.account_id = @acctrab.id
  end
end
