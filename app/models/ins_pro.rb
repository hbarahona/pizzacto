class InsPro < ApplicationRecord
  belongs_to :producto, :foreign_key => :inpr_prod, :primary_key => :pro_id
  belongs_to :insumo, :foreign_key => :inpr_insu, :primary_key => :ins_id
end
