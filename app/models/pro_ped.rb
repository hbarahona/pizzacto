class ProPed < ApplicationRecord
  belongs_to :producto, :foreign_key => :prpe_prod, :primary_key => :pro_id
  belongs_to :pedido, :foreign_key => :prpe_pedi, :primary_key => :ped_id
end
