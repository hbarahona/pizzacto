class Stock < ApplicationRecord
  belongs_to :insumo, :foreign_key => :sto_insu, :primary_key => :ins_id
  belongs_to :pizzeria, :foreign_key => :sto_pizz, :primary_key => :piz_id
end
