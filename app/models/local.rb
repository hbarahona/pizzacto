class Local < ApplicationRecord
  self.table_name = "pizzeria"
  has_many :stock, :foreign_key => :sto_pizz, :primary_key => :piz_id
  has_many :trabajador, :foreign_key => :tra_pizz, :primary_key => :piz_id
  has_many :pedido, :foreign_key => :ped_pizz, :primary_key => :piz_id


end
