class Cliente < ApplicationRecord
  has_many :pedido, :foreign_key => :ped_clie, :primary_key => :cli_id
  belongs_to :account
end
