class Insumo < ApplicationRecord
  has_many :ins_pro, :foreign_key => :inpr_insu, :primary_key => :ins_id
  has_many :stock, :foreign_key => :sto_insu, :primary_key => :ins_id
end
