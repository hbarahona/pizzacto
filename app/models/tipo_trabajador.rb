class TipoTrabajador < ApplicationRecord
  has_many :trabajador, :foreign_key => :tra_ttra, :primary_key => :ttra_id

  validates :ttra_id, uniqueness: true
end
