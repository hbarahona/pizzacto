class Producto < ApplicationRecord
  belongs_to :tipo_producto, :foreign_key => :pro_tpro, :primary_key => :tpro_id
  has_many :ins_pro, :foreign_key => :inpr_prod, :primary_key => :pro_id, dependent: :destroy, inverse_of: :producto
  has_many :pro_ped, :foreign_key => :prpe_prod, :primary_key => :pro_id

  #accepts_nested_attributes_for :ins_pro, :reject_if => lambda { |b| b[:inpr_insu].blank? }
  accepts_nested_attributes_for :ins_pro, allow_destroy: true, reject_if: ->(attrs) { attrs['inpr_cant'].blank? || attrs['inpr_insu'].blank? }
end
