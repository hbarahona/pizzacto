class Pedido < ApplicationRecord
  belongs_to :estado, :foreign_key => :ped_esta, :primary_key => :est_id
  belongs_to :local, :foreign_key => :ped_pizz, :primary_key => :piz_id
  belongs_to :trabajador, :foreign_key => :ped_trab, :primary_key => :tra_rut
  belongs_to :cliente, :foreign_key => :ped_clie,  :primary_key => :cli_id
  has_many :pro_ped, :foreign_key => :prpe_pedi, :primary_key => :ped_id, dependent: :destroy, inverse_of: :pedido

  accepts_nested_attributes_for :pro_ped, allow_destroy: true, reject_if: ->(attrs) { attrs['prpe_cant'].blank? || attrs['prpe_prod'].blank? }
end
