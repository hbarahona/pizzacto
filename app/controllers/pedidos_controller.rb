class PedidosController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]

  # GET /pedidos
  def index

    if is_administrador
      @pedidos = Pedido.all
      @cant = @pedidos.count
    elsif is_trabajador
      @cant = current_account.trabajador.pedido.count
      if @cant > 0
        @pedidos = current_account.trabajador.pedido.all
      end
    elsif is_cliente
      @cant = current_account.cliente.pedido.count
      if @cant > 0
        @pedidos = current_account.cliente.pedido.all
      end
    end
    @pedido = Pedido.new
    1.times { @pedido.pro_ped.build }
  end

  # GET /pedidos/1
  def show
  end

  # GET /pedidos/new
  def new
    @pedido = Pedido.new
  end

  # GET /pedidos/1/edit
  def edit
  end

  # POST /pedidos
  def create
    @pedido = Pedido.new(pedido_params)
    if @pedido.save
      redirect_to pedidos_url, notice: 'Pedido was successfully created.'
    else
        redirect_to pedidos_url
    end
  end

  # PATCH/PUT /pedidos/1
  def update
    if @pedido.update(pedido_params)
        redirect_to pedidos_url, notice: 'Pedido was successfully updated.'
    else
        redirect_to pedidos_url
    end
  end

  # DELETE /pedidos/1
  def destroy
    @pedido.destroy
      redirect_to pedidos_url notice: 'Pedido was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedido
      @pedido = Pedido.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pedido_params
      params.require(:pedido).permit(:ped_tota, :ped_fecha, :ped_esta, :ped_pizz, :ped_trab, :ped_clie, pro_ped_attributes: [:prpe_prod, :prpe_pedi, :prpe_cant, :_destroy])
    end
end
