class PageController < ApplicationController
  before_action :authenticate_account!, except: [:index]

  def index
  end

  def auditoria
    @audit = AuditTables
  end

  def mtb
    @mtb = Tipo_trabajador
    @cod = params[:cod]
    @desc = params[:dec]
    @tran = params[:tran]
    plsql.PRO_TTRA_MAN(@cod,@desc,@tran)

  end



  def procedimiento
  end

  def reporte
    plsql.PRO_RESUMEN_VENTAS
    @proc = ResumenVentas.all
  end

  def vista
    @vista1 = VVenTotal.all
    @vista2 = VInsLocal.all

    data=[]
    label=[]
    color =[]

    @vista1.each do |v1|
      color.append "rgba(#{rand(0..255)},#{rand(0..255)},#{rand(0..255)},0,1}"
      data.append v1.total
      label.append 'local'+v1.ped_pizz.to_s
    end
    @datos = {datasets: [{data: data },{background_color: color}],labels: label, };
  end
end
