class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_account!

  helper_method :is_cliente, :is_trabajador, :is_administrador

  def is_cliente
    current_account.rol == 'cli'
  end

  def is_trabajador
    current_account.rol == 'tra'
  end

  def is_administrador
    current_account.rol == 'adm'
  end

end
