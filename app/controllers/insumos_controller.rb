class InsumosController < ApplicationController
  before_action :set_insumo, only: [:show, :edit, :update, :destroy]

  # GET /insumos
  def index
    @insumos = Insumo.all
    #if (Insumo.find(params[:id]))
    #  @insumo = Insumo.find(params[:id])
    #else
      @insumo = Insumo.new
    #end
  end

  # GET /insumos/1
  def show
  end

  # GET /insumos/new
  def new
    @insumo = Insumo.new
  end

  # GET /insumos/1/edit
  def edit
  end

  # POST /insumos
  def create
    @insumo = Insumo.new(insumo_params)

    if @insumo.save
      redirect_to insumos_url, notice: 'Insumo creado.'
    else
      redirect_to insumos_url, notice: 'No fue posible crear el Insumo.'
    end
  end

  # PATCH/PUT /insumos/1
  def update
    if @insumo.update(insumo_params)
      redirect_to insumos_url, notice: 'Insumo was successfully updated.'
    else
      redirect_to insumos_url, notice: 'Insumo no actualizado.'
    end
  end

  # DELETE /insumos/1
  def destroy
    @insumo.destroy
    redirect_to insumos_url, notice: 'Insumo was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_insumo
      @insumo = Insumo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def insumo_params
      params.require(:insumo).permit(:ins_nomb)
    end
end
