class ProPedsController < ApplicationController
  before_action :set_pro_ped, only: [:show, :edit, :update, :destroy]

  # GET /pro_peds
  def index
    @pro_peds = ProPed.all
  end

  # GET /pro_peds/1
  def show
  end

  # GET /pro_peds/new
  def new
    @pro_ped = ProPed.new
  end

  # GET /pro_peds/1/edit
  def edit
  end

  # POST /pro_peds
  def create
    @pro_ped = ProPed.new(pro_ped_params)

    if @pro_ped.save
      redirect_to @pro_ped, notice: 'Pro ped was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pro_peds/1
  def update
    if @pro_ped.update(pro_ped_params)
      redirect_to @pro_ped, notice: 'Pro ped was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pro_peds/1
  def destroy
    @pro_ped.destroy
    redirect_to pro_peds_url, notice: 'Pro ped was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pro_ped
      @pro_ped = ProPed.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pro_ped_params
      params.require(:pro_ped).permit(:prpe_pedi_id, :prpe_prod_id, :prpe_cant)
    end
end
