class TipoTrabajadorsController < ApplicationController
  before_action :set_tipo_trabajador, only: [:show, :edit, :update, :destroy]

  # GET /tipo_trabajadors
  def index
    @tipo_trabajadors = TipoTrabajador.all
    @tipo_trabajador = TipoTrabajador.new
  end

  # GET /tipo_trabajadors/1
  def show
  end

  # GET /tipo_trabajadors/new
  def new
    @tipo_trabajador = TipoTrabajador.new
  end

  # GET /tipo_trabajadors/1/edit
  def edit
  end

  # POST /tipo_trabajadors
  def create
    #@tipo_trabajador = TipoTrabajador.new(tipo_trabajador_params)
    @identificador = params[:tipo_trabajador][:ttra_id]
    @descripcion = params[:tipo_trabajador][:ttra_desc]
    @opcion = params[:tipo_trabajador][:opcion]
    plsql.PRO_TTRA_MAN(@identificador, @descripcion,@opcion)

    #if @tipo_trabajador.save
      redirect_to tipo_trabajadors_url, notice: 'Tipo trabajador was successfully created.'
    #else
      #redirect_to tipo_trabajadors_url, notice: 'E'
    #end
  end

  # PATCH/PUT /tipo_trabajadors/1
  def update
    #if @tipo_trabajador.update(tipo_trabajador_params)
      @identificador = params[:tipo_trabajador][:ttra_id]
      @descripcion = params[:tipo_trabajador][:ttra_desc]
      plsql.PRO_TTRA_MAN(@identificador, @descripcion, "A")
      redirect_to tipo_trabajadors_url, notice: 'Tipo trabajador was successfully updated.'
    #else
      #redirect_to tipo_trabajadors_url, notice: 'E'
    #end
  end

  # DELETE /tipo_trabajadors/1
  def destroy
    ##@tipo_trabajador.destroy
    @identificador = params[:tipo_trabajador][:ttra_id]
    @descripcion = params[:tipo_trabajador][:ttra_desc]
      plsql.PRO_TTRA_MAN(@identificador, @descripcion,"E")
    redirect_to tipo_trabajadors_url, notice: 'Tipo trabajador was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_trabajador
      @tipo_trabajador = TipoTrabajador.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_trabajador_params
      params.require(:tipo_trabajador).permit(:ttra_id, :ttra_desc, :opcion)
    end
end
