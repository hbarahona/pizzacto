class InsProsController < ApplicationController
  before_action :set_ins_pro, only: [:show, :edit, :update, :destroy]

  # GET /ins_pros
  def index
    @ins_pros = InsPro.all
  end

  # GET /ins_pros/1
  def show
  end

  # GET /ins_pros/new
  def new
    @ins_pro = InsPro.new
  end

  # GET /ins_pros/1/edit
  def edit
  end

  # POST /ins_pros
  def create
    @ins_pro = InsPro.new(ins_pro_params)

    if @ins_pro.save
      redirect_to @ins_pro, notice: 'Ins pro was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /ins_pros/1
  def update
    if @ins_pro.update(ins_pro_params)
      redirect_to @ins_pro, notice: 'Ins pro was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /ins_pros/1
  def destroy
    @ins_pro.destroy
    redirect_to ins_pros_url, notice: 'Ins pro was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ins_pro
      @ins_pro = InsPro.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ins_pro_params
      params.require(:ins_pro).permit(:inpr_prod_id, :inpr_insu_id, :inpr_cant)
    end
end
