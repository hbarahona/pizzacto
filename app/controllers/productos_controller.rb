class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]

  # GET /productos
  def index
    @productos = Producto.all
    @producto = Producto.new
    1.times { @producto.ins_pro.build }
  end

  # GET /productos/1
  def show
  end

  # GET /productos/new
  def new
    @producto = Producto.new
  end

  # GET /productos/1/edit
  def edit
  end

  # POST /productos
  def create
    @producto = Producto.new(producto_params)
    if @producto.save
      redirect_to productos_url, notice: 'Producto was successfully created.'
    else
      redirect_to productos_url, notice: 'Producto was successfully updated.'
    end
  end

  # PATCH/PUT /productos/1
  def update
    if @producto.update(producto_params)
      redirect_to productos_url, notice: 'Producto was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /productos/1
  def destroy
    @producto.destroy
    redirect_to productos_url, notice: 'Producto was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producto_params
      params.require(:producto).permit(:pro_desc, :pro_prec, :pro_tpro, ins_pro_attributes: [:inpr_prod, :inpr_insu, :inpr_cant, :_destroy])
    end
end
