class TipoProductosController < ApplicationController
  before_action :set_tipo_producto, only: [:show, :edit, :update, :destroy]

  # GET /tipo_productos
  def index
    @tipo_productos = TipoProducto.all
    @tipo_producto = TipoProducto.new
  end

  # GET /tipo_productos/1
  def show
  end

  # GET /tipo_productos/new
  def new
    @tipo_producto = TipoProducto.new
  end

  # GET /tipo_productos/1/edit
  def edit
  end

  # POST /tipo_productos
  def create
    @tipo_producto = TipoProducto.new(tipo_producto_params)

    if @tipo_producto.save
      redirect_to @tipo_producto, notice: 'Tipo producto was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tipo_productos/1
  def update
    if @tipo_producto.update(tipo_producto_params)
      redirect_to @tipo_producto, notice: 'Tipo producto was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tipo_productos/1
  def destroy
    @tipo_producto.destroy
    redirect_to tipo_productos_url, notice: 'Tipo producto was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_producto
      @tipo_producto = TipoProducto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_producto_params
      params.require(:tipo_producto).permit(:tro_id, :tpro_desc)
    end
end
