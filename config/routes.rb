Rails.application.routes.draw do
  devise_for :accounts
  resources :pro_peds
  resources :locals
  resources :tipo_trabajadors
  resources :pedidos
  resources :trabajadors
  resources :stocks
  resources :ins_pros
  resources :clientes
  resources :insumos
  resources :estados
  resources :productos
  resources :tipo_productos
  root 'page#index'
  get 'page/auditoria'
  get 'page/mtb'
  get 'page/procedimiento'
  get 'page/reporte'
  get 'page/vista'
  post 'page/mtb'
end
