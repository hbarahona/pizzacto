require 'test_helper'

class TrabajadorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trabajador = trabajador(:one)
  end

  test "should get index" do
    get trabajadors_url
    assert_response :success
  end

  test "should get new" do
    get new_trabajador_url
    assert_response :success
  end

  test "should create trabajador" do
    assert_difference('Trabajador.count') do
      post trabajadors_url, params: { trabajador: { tra_apma: @trabajador.tra_apma, tra_appa: @trabajador.tra_appa, tra_nomb: @trabajador.tra_nomb, tra_pass: @trabajador.tra_pass, tra_pizz_id: @trabajador.tra_pizz_id, tra_ttra_id: @trabajador.tra_ttra_id } }
    end

    assert_redirected_to trabajador_url(Trabajador.last)
  end

  test "should show trabajador" do
    get trabajador_url(@trabajador)
    assert_response :success
  end

  test "should get edit" do
    get edit_trabajador_url(@trabajador)
    assert_response :success
  end

  test "should update trabajador" do
    patch trabajador_url(@trabajador), params: { trabajador: { tra_apma: @trabajador.tra_apma, tra_appa: @trabajador.tra_appa, tra_nomb: @trabajador.tra_nomb, tra_pass: @trabajador.tra_pass, tra_pizz_id: @trabajador.tra_pizz_id, tra_ttra_id: @trabajador.tra_ttra_id } }
    assert_redirected_to trabajador_url(@trabajador)
  end

  test "should destroy trabajador" do
    assert_difference('Trabajador.count', -1) do
      delete trabajador_url(@trabajador)
    end

    assert_redirected_to trabajadors_url
  end
end
