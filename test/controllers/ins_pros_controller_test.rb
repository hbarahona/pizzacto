require 'test_helper'

class InsProsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ins_pro = ins_pro(:one)
  end

  test "should get index" do
    get ins_pros_url
    assert_response :success
  end

  test "should get new" do
    get new_ins_pro_url
    assert_response :success
  end

  test "should create ins_pro" do
    assert_difference('InsPro.count') do
      post ins_pros_url, params: { ins_pro: { inpr_cant: @ins_pro.inpr_cant, inpr_insu_id: @ins_pro.inpr_insu_id, inpr_prod_id: @ins_pro.inpr_prod_id } }
    end

    assert_redirected_to ins_pro_url(InsPro.last)
  end

  test "should show ins_pro" do
    get ins_pro_url(@ins_pro)
    assert_response :success
  end

  test "should get edit" do
    get edit_ins_pro_url(@ins_pro)
    assert_response :success
  end

  test "should update ins_pro" do
    patch ins_pro_url(@ins_pro), params: { ins_pro: { inpr_cant: @ins_pro.inpr_cant, inpr_insu_id: @ins_pro.inpr_insu_id, inpr_prod_id: @ins_pro.inpr_prod_id } }
    assert_redirected_to ins_pro_url(@ins_pro)
  end

  test "should destroy ins_pro" do
    assert_difference('InsPro.count', -1) do
      delete ins_pro_url(@ins_pro)
    end

    assert_redirected_to ins_pros_url
  end
end
