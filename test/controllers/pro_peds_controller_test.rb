require 'test_helper'

class ProPedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pro_ped = pro_ped(:one)
  end

  test "should get index" do
    get pro_peds_url
    assert_response :success
  end

  test "should get new" do
    get new_pro_ped_url
    assert_response :success
  end

  test "should create pro_ped" do
    assert_difference('ProPed.count') do
      post pro_peds_url, params: { pro_ped: { prpe_cant: @pro_ped.prpe_cant, prpe_pedi_id: @pro_ped.prpe_pedi_id, prpe_prod_id: @pro_ped.prpe_prod_id } }
    end

    assert_redirected_to pro_ped_url(ProPed.last)
  end

  test "should show pro_ped" do
    get pro_ped_url(@pro_ped)
    assert_response :success
  end

  test "should get edit" do
    get edit_pro_ped_url(@pro_ped)
    assert_response :success
  end

  test "should update pro_ped" do
    patch pro_ped_url(@pro_ped), params: { pro_ped: { prpe_cant: @pro_ped.prpe_cant, prpe_pedi_id: @pro_ped.prpe_pedi_id, prpe_prod_id: @pro_ped.prpe_prod_id } }
    assert_redirected_to pro_ped_url(@pro_ped)
  end

  test "should destroy pro_ped" do
    assert_difference('ProPed.count', -1) do
      delete pro_ped_url(@pro_ped)
    end

    assert_redirected_to pro_peds_url
  end
end
